from cnn_manager import CNNManager

ccn_model = CNNManager(
    file_name="model_cifar_epoch=10_batch=64_lr=0.01.h5"
)

ccn_model.model_summery()

# NOTE: Use only images of the categories
# 'Apple Golden 1', 'Avocado', 'Banana', 'Cherry 1', 'Cocos', 'Kiwi', 'Lemon', 'Mango' and 'Orange'
ccn_model.predict_image(
    image_path="C:/Users/marti/Downloads/input/fruits-360_dataset/fruits-360/Test/Banana/17_100.jpg"
)
ccn_model.predict_image(
    image_path="C:/Users/marti/Downloads/input/fruits-360_dataset/fruits-360/Test/Lemon/r_25_100.jpg"
)
