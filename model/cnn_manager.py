# ~~~~~ IMPORTING THE REQUIRED LIBRARIES
import numpy as np
import tensorflow as tf

from keras.models import load_model
from PIL import Image


class CNNManager:
    def __init__(self,
                 file_name: str = None):
        self.model = None

        if file_name is None:
            # TODO: Throw an error
            print('ERROR')
        else:
            self.load_model(file_name=file_name)

    def load_model(self, file_name):
        self.model = load_model('classification_model\\models\\{}'.format(file_name))
        print(f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              f'\n--> Model \'{file_name}\' loaded'
              f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

    def model_summery(self):
        # ~~~~~ SHOWS THE SUMMERY OF THE MODEL
        print(f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              f'\n--> Model summery'
              f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        self.model.summary()

    def predict_image(self, image_path):
        # TODO: Embed into a database
        # labels = {
        #     0: 'airplane',
        #     1: 'automobile',
        #     2: 'bird',
        #     3: 'cat',
        #     4: 'deer',
        #     5: 'dog',
        #     6: 'frog',
        #     7: 'horse',
        #     8: 'ship',
        #     9: 'truck'
        # }
        labels = ['Apple Golden 1', 'Avocado', 'Banana', 'Cherry 1', 'Cocos', 'Kiwi', 'Lemon', 'Mango', 'Orange']

        image = Image.open(image_path)
        # image = image.resize((32, 32))
        image = np.expand_dims(image, axis=0)
        image = np.array(image)

        probability_model = tf.keras.Sequential([self.model,
                                                 tf.keras.layers.Softmax()])
        prediction = probability_model.predict(image)
        prediction = np.argmax(prediction[0])

        # prediction = np.argmax(self.model.predict([image])[0], axis=-1)
        print(f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              f'\n--> Image path.....{image_path}'
              f'\n--> Prediction.....{prediction}'
              f'\n--> Label..........{labels[prediction]}'
              f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

        return labels[int(prediction)]
