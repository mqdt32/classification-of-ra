# ~~~~~ IMPORTING THE REQUIRED LIBRARIES
import os
import cv2
import random
import numpy as np
import matplotlib.pyplot as pp

from keras.datasets import cifar10
from keras.utils import np_utils, to_categorical


class Dataset:
    def __init__(self,
                 dataset_label=None,
                 dataset_path_train=None,
                 dataset_path_test=None,
                 image_size=100):
        self.dataset_label = dataset_label
        self.dataset_path_train = dataset_path_train
        self.dataset_path_test = dataset_path_test
        self.image_size = image_size

        self.labels: list = []
        self.x_train: list = []
        self.x_test: list = []
        self.y_train: list = []
        self.y_test: list = []
        self.num_classes: int = 0

        if self.dataset_label is None:
            self.load_keras_cifar10()
        else:
            self.collect_labels()

    def load_keras_cifar10(self):
        self.dataset_label = 'cifar10'

        # ~~~~~ LOAD THE DATASET FROM 'keras'
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()

        # ~~~~~ CONVERT THE PIXEL VALUES TO FLOAT TYPE AND NORMALIZE THE DATASET
        self.x_train = (x_train.astype('float32')) / 255.0
        self.x_test = (x_test.astype('float32')) / 255.0

        self.y_train = self.target_class_encoding(y_train)
        self.y_test = self.target_class_encoding(y_test)

    def target_class_encoding(self, y):
        # ~~~~~ PERFORM ONE-HOT ENCODING FOR THE TARGET CLASSES
        # y = np_utils.to_categorical(y)

        y = to_categorical(y, num_classes=len(self.labels))

        if self.num_classes < y.shape[1]:
            self.num_classes = y.shape[1]

        return y

    def collect_labels(self):
        # ~~~~~ CHECK IF PATH TO TRAIN AND TEST DATASET IS VALID
        if self.dataset_path_train is None or self.dataset_path_test is None:
            print('!!! ERROR !!! Path invalid: Please try another path')
            return

        # TODO: python excepts only paths with a forward slash (/) -> convert paths with a backslash
        labels_train = os.listdir(self.dataset_path_train)
        labels_test = os.listdir(self.dataset_path_test)
        if [i for i in labels_train + labels_test if i not in labels_train or i not in labels_test]:
            print('!!! ERROR !!! Please make sure the train and test dataset contains the same labels')
            return

        self.labels = labels_train
        # ~~~~~ FOR TEST PURPOSES ONLY
        self.labels = ['Apple Golden 1', 'Avocado', 'Banana', 'Cherry 1', 'Cocos', 'Kiwi', 'Lemon', 'Mango', 'Orange']
        # ~~~~~ !!!
        self.collect_raw_images()

    def collect_raw_images(self):
        array_train = []
        array_test = []

        for index in self.labels:
            p_train = os.path.join(self.dataset_path_train, index)
            p_test = os.path.join(self.dataset_path_test, index)
            label_index = self.labels.index(index)

            # ~~~~~ COLLECTING THE RAW IMAGES FROM THE DEPOSITED PATHS
            for image in os.listdir(p_train):
                image_array = cv2.imread(os.path.join(p_train, image))
                image_array = cv2.cvtColor(image_array, cv2.COLOR_BGR2RGB)
                array_train.append([image_array, label_index])
            for image in os.listdir(p_test):
                image_array = cv2.imread(os.path.join(p_test, image))
                image_array = cv2.cvtColor(image_array, cv2.COLOR_BGR2RGB)
                array_test.append([image_array, label_index])

        self.prepare_data(array_train, array_test)

    def prepare_data(self, a_train, a_test):
        # ~~~~~ CREATE X FEATURES AND Y LABELS WITH SHUFFLE FUNCTION TO MIX THE DATASET
        random.shuffle(a_train)
        for feature, label in a_train:
            self.x_train.append(feature)
            self.y_train.append(label)
        self.x_train = np.array(self.x_train)
        random.shuffle(a_test)
        for feature, label in a_test:
            self.x_test.append(feature)
            self.y_test.append(label)
        self.x_test = np.array(self.x_test)

        # ~~~~~ NORMALIZE, RESHAPE AND ADD RGB SCALE
        self.x_train = self.x_train.reshape(-1, self.image_size, self.image_size, 3)
        self.x_train = self.x_train / 255
        self.x_test = self.x_test.reshape(-1, self.image_size, self.image_size, 3)
        self.x_test = self.x_test / 255

        # ~~~~~ CONVERT LABELS TO ONE-HOT ENCODING
        self.y_train = self.target_class_encoding(self.y_train)
        self.y_test = self.target_class_encoding(self.y_test)
        # TODO: How can the processed be saved so that they do not have to be processed each time form scratch?

    # TODO: Useful???
    def visualize_dataset_sample(self, N=6):
        # ~~~~~ VISUALIZE THE DATASET
        pp.figure(figsize=(10, 5))
        for i in range(N):
            pp.subplot(330 + 1 + i)
            pp.imshow(self.x_train[i])
        pp.show()

    def get_x_train(self):
        return self.x_train

    def get_x_test(self):
        return self.x_test

    def get_y_train(self):
        return self.y_train

    def get_y_test(self):
        return self.y_test

    def get_dataset_label(self):
        return self.dataset_label

    def get_num_classes(self):
        return self.num_classes
