from dataset_preperation import Dataset
from cnn import CNN

dataset = Dataset(
    dataset_label='fruits360',
    dataset_path_test='C:/Users/marti/Downloads/input/fruits-360_dataset/fruits-360/Test',
    dataset_path_train='C:/Users/marti/Downloads/input/fruits-360_dataset/fruits-360/Training'
)

cnn = CNN(
    dataset=dataset,
    learning_rate=0.01,
    epochs=10,
    batch_size=64
)

cnn.create_model()
cnn.model_accuracy()
cnn.save_model()
