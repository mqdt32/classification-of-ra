# ~~~~~ IMPORTING THE REQUIRED LIBRARIES
from dataset_preperation import Dataset
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
# from keras.constraints import maxnorm
from keras.optimizers import SGD


class CNN:
    def __init__(self,
                 load_dataset: bool = False,
                 dataset: Dataset = None,
                 learning_rate: float = 0.01,
                 epochs: int = 5,
                 batch_size: int = 32):
        """
        The CNN class trains and tests a Convolutional Neural Network, which is used to classify images. More
        information about the architecture of the model can be found in the documentation.
        Furthermore, the model can be saved and loaded.

        Parameters
        ----------
        load_dataset: False if no specific dataset is selected (in this case a default Keras dataset is used)
        dataset: Represent the dataset of the class Dataset on which the model is trained
        learning_rate: Defines the parameter that determines the step size at each iteration
        epochs: Number of times the complete data set is run through
        batch_size: Defines how many images are trained on per update
        """
        self.model = None

        if load_dataset:
            self.dataset = Dataset()
        else:
            self.dataset = dataset

        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.epochs = epochs

    def create_model(self):
        """
        Creates the model according to the architecture defined in the documentation. The model is based on a sequential
        model.

        Parameters
        ----------

        Returns
        -------
        """
        # ~~~~~ CREATE THE SEQUENTIAL MODEL
        self.model = Sequential()

        # ~~~~~ ADD FEATURE EXTRACTION LAYERS
        self.model.add(Conv2D(
            filters=8, kernel_size=(3, 3), padding='same', activation='relu', input_shape=(100, 100, 3)
        ))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.35))
        self.model.add(Conv2D(
            filters=16, kernel_size=(3, 3), padding='same', activation='relu'
        ))
        self.model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
        self.model.add(Dropout(0.35))
        self.model.add(Conv2D(
            filters=32, kernel_size=(3, 3), padding='same', activation='relu'
        ))
        self.model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
        self.model.add(Dropout(0.35))

        # ~~~~~ ADD CLASSIFICATION LAYERS
        self.model.add(Flatten())
        self.model.add(Dense(512, activation='relu'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(9, activation='softmax'))

        print(f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              f'\n--> Model created'
              f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

        self.configure_optimizer()
        self.train_model()

    def configure_optimizer(self):
        """
        Optimizes the CNN-model using a gradient descent optimizer (with momentum).

        Parameters
        ----------

        Returns
        -------
        """
        # ~~~~~ CONFIGURE THE OPTIMIZER AND COMPILE THE MODEL
        sgd = SGD(learning_rate=self.learning_rate, momentum=0.9, decay=(0.01 / 25), nesterov=False)

        self.model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
        print(f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              f'\n--> Optimizer configured'
              f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

    def train_model(self):
        """
        Trains the model with the selected dataset. The parameters epochs and batch_size are taken into account.

        Parameters
        ----------

        Returns
        -------
        """
        print(f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              f'\n--> Start training the model'
              f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        self.model.fit(self.dataset.get_x_train(), self.dataset.get_y_train(),
                       validation_data=(self.dataset.get_x_test(), self.dataset.get_y_test()), epochs=self.epochs,
                       batch_size=self.batch_size)
        print(f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              f'\n--> End training the model'
              f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

    def model_accuracy(self):
        """
        The method calculates the accuracy of the model using a test dataset.

        Parameters
        ----------

        Returns
        -------
        """
        # ~~~~~ CALCULATE MODEL ACCURACY ON TESTING DATA
        if self.model is None:
            print('!!! ERROR !!! Please load/create a model')
            return
        if self.dataset is None:
            print('!!! ERROR !!! No dataset available')
            return

        print(f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              f'\n--> Compute model accuracy'
              f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        _, acc = self.model.evaluate(self.dataset.get_x_test(), self.dataset.get_y_test())
        print(f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              f'\n--> Model accuracy.....{acc*100: .2f}%'
              f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

    def save_model(self):
        """
        Archives the model for later use.

        Parameters
        ----------

        Returns
        -------
        """
        if self.epochs < 10:
            epoch_str = '0' + str(self.epochs)
        else:
            epoch_str = str(self.epochs)
        if self.batch_size < 10:
            batch_str = '0' + str(self.batch_size)
        else:
            batch_str = str(self.batch_size)
        lr_str = str(self.learning_rate)
        ds_str = str(self.dataset.get_dataset_label())

        if self.model is None:
            print('!!! ERROR !!! Please load/create a model')
            return

        self.model.save('classification_model\\models\\'
                        'model_cifar_epoch={}_batch={}_lr={}.h5'.format(epoch_str, batch_str, lr_str))
        print(f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              f'\n--> Model \'model_{ds_str}_epoch={epoch_str}_batch={batch_str}_lr={lr_str}\' saved'
              f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

    def load_model(self, file_name):
        """
        Loads an already trained and optimized CNN-model for image classification.

        Parameters
        ----------
        file_name: File name of the model

        Returns
        -------
        """
        if file_name is None:
            print('!!! ERROR !!! Please enter a file name')
            return

        self.model = load_model('classification_model\\models\\{}'.format(file_name))
        print(f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              f'\n--> Model \'{file_name}\' loaded'
              f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
